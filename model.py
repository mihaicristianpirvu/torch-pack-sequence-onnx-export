"""Users-sessions recurrent lstm architecture v1."""
from typing import Dict, List, Tuple
import torch as tr
from torch import nn
from torch.nn.utils.rnn import pack_sequence, unpack_sequence
from torch.nn import functional as F


class ModelDynamic(nn.Module):
    def __init__(self, input_shape: Dict[str, int], hidden_shape: int, output_shape: int):
        super().__init__()
        self._input_shape = input_shape
        self.hidden_shape = hidden_shape
        self._output_shape = output_shape

        # sessions layer: variable size lstm cell taking sessions as inputs
        self.lstm = nn.LSTM(input_size=input_shape["sessions"], hidden_size=hidden_shape)
        # users-sessions layer: linear layer taking users-sessions as inputs
        self.out_layer = nn.Linear(in_features=hidden_shape + input_shape["users"], out_features=output_shape)

    def forward(self, x_users: tr.Tensor, x_sessions: List[tr.Tensor]) -> tr.Tensor:
        """Forward pass."""
        # Pack and pass through the sessions LSTM for this batch
        x_packed = pack_sequence(x_sessions, enforce_sorted=False)
        y_full_packed, y_sessions = self.lstm.forward(x_packed)
        # unnecessary here, but if we want to stack more lstms together, we need it.
        y_sessions_full = unpack_sequence(y_full_packed)

        # Concatenate the LSTM's output with the users features
        x_users_sessions = tr.cat([x_users, y_sessions[0][0]], dim=1)
        y_out = self.out_layer(x_users_sessions)
        return y_out

class ModelDynamicManual(nn.Module):
    def __init__(self, input_shape: Dict[str, int], hidden_shape: int, output_shape: int):
        super().__init__()
        self._input_shape = input_shape
        self.hidden_shape = hidden_shape
        self._output_shape = output_shape

        # sessions layer: variable size lstm cell taking sessions as inputs
        self.lstm = nn.LSTM(input_size=input_shape["sessions"], hidden_size=hidden_shape)
        # users-sessions layer: linear layer taking users-sessions as inputs
        self.out_layer = nn.Linear(in_features=hidden_shape + input_shape["users"], out_features=output_shape)

    @staticmethod
    def pad(x_data: List[tr.Tensor]) -> Tuple[tr.Tensor, tr.Tensor]:
        lens = tr.LongTensor([y.size(0) for y in x_data])
        bs = len(x_data)
        n_feats = x_data[0].size(1)
        diffs = lens.max() - lens
        x_sessions_padded = tr.zeros(bs, lens.max(), n_feats)
        for i in range(bs):
            x_sessions_padded[i] = F.pad(x_data[i], (0, 0, 0, int(diffs[i])))
        x_sessions_padded = x_sessions_padded.permute(1, 0, 2)
        return x_sessions_padded, lens

    def forward(self, x_users: tr.Tensor, x_sessions: List[tr.Tensor]) -> tr.Tensor:
        """Forward pass."""
        # pad and unpad manually with zeros at the end
        x_sessions_padded, lens = ModelDynamicManual.pad(x_sessions)
        y_sessions = self.lstm(x_sessions_padded)[0]
        y_sessions = y_sessions[lens - 1, tr.arange(len(x_sessions))]

        # Concatenate the LSTM's output with the users features
        x_users_sessions = tr.cat([x_users, y_sessions], dim=1)
        y_out = self.out_layer(x_users_sessions)
        return y_out
