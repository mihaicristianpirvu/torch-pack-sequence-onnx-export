# ONNX-exporter for `pack_sequence` and `unpack_sequence`

Pytorch version: 1.13.1

## Situation:

we have a dynamic shaped RNN with input of shape (MB, ?, n_feats) and we want to export to ONNX. We use `pad_sequence` and `unpad_sequence` in our torch code with no issue. However, torch.onnx.export complains about this.

## How to run

Open `main.ipynb` and press run all or watch it in nbviewer: [link](https://nbviewer.org/urls/gitlab.com/mihaicristianpirvu/torch-pack-sequence-onnx-export/-/raw/master/main.ipynb)

## A bit more detail

We have a recurrent model that processes a single non-batched sequence:

- `y = rnn(x_seq)` with `x_seq::(t, n_feats)` and `y::(m_feats)`

All is good but when we use mini-batches, each batch has it's own sequence size T:

- `Y = rnn(X)` with `X::(MB, ?, n_feats)`
- Assuming we have `MB=2` and `T=[3, 9]` (first sequence has 3 steps, 2nd has 9 steps)
- The RNN doesn't understand this, so we circumvent it by using [pack_sequence](https://pytorch.org/docs/stable/generated/torch.nn.utils.rnn.pack_sequence.html) and `unpack_sequence`
- `X' = pack_sequence(X); Y' = rnn(X'); Y = unpack_sequence(Y')` (briefly)
- This takes care behind the scenes such that y[0] is at the 3rd step and y[1] is at the 9th step.What happens is that it does a padding with zeros + a converting to a special internal torch class `PackedSequence` that linearizes the entire sequence and keeps track of lengths of the original sequences.

**HOWEVER** when we export this to ONNX we are greatly met with:

```'RuntimeError: Dynamic shape axis should be no more than the shape dimension for feature_size'```

We try to use `torch.jit.script(model)` and then `torch.onnx.export` and we are met with:

```'RuntimeError: r INTERNAL ASSERT FAILED at "../aten/src/ATen/core/jit_type_base.h":547, please report a bug to PyTorch.'```

Frustration grows, but we don't give up :) padding and packing shouldn't be that hard, so we write our own zero-padding code, pass the padded data to the lstm and unpad it. No packing is necessary in any special structure, just torch tensors. See `model.ModelDynamicManual`
